import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;
import scpsolver.constraints.LinearBiggerThanEqualsConstraint;
import scpsolver.constraints.LinearSmallerThanEqualsConstraint;
import scpsolver.lpsolver.LinearProgramSolver;
import scpsolver.lpsolver.SolverFactory;
import scpsolver.problems.LPSolution;
import scpsolver.problems.LPWizard;
import scpsolver.problems.LinearProgram;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by vklein on 08/09/15.
 */
public class SimpleTest {

    public static void main(String[] args) {

        //A test SCP
        double[] sol;

        //Low level interface
        LinearProgram lp = new LinearProgram(new double[]{5.0,10.0});
        lp.addConstraint(new LinearBiggerThanEqualsConstraint(new double[]{3.0,1.0}, 8.0, "c1"));
        lp.addConstraint(new LinearBiggerThanEqualsConstraint(new double[]{0.0,4.0}, 4.0, "c2"));
        lp.addConstraint(new LinearSmallerThanEqualsConstraint(new double[]{2.0,0.0}, 2.0, "c3"));
        lp.setMinProblem(true);
        LinearProgramSolver solver  = SolverFactory.newDefault();
        //double[] sol = solver.solve(lp);
        //System.out.println("x1:" + sol[0] + " x2:" + sol[1]);

        //High level interface
        LPWizard lpw = new LPWizard();
        lpw.plus("x1", 5.0).plus("x2",10.0);
        lpw.addConstraint("c1",8,"<=").plus("x1", 3.0).plus("x2",1.0);
        lpw.addConstraint("c2", 4, "<=").plus("x2",4.0);
        lpw.addConstraint("c3", 2, ">=").plus("x1", 2.0);
        lpw.setMinProblem(true);
        LPSolution lpSolution = lpw.solve();
        System.out.println(lpSolution.toString());


        //B Test combinatorics
        List<String> ltest = new ArrayList<String>(Arrays.asList(new String[]{"a", "b", "c", "d", "e"}));
        ICombinatoricsVector<String> initialSet =  Factory.createVector(ltest);
        Generator<String> gen = Factory.createSubSetGenerator(initialSet);

        System.out.println(gen.getNumberOfGeneratedObjects());
        for(ICombinatoricsVector<String> subset:gen) {
            if(subset.getSize() == 2)
                System.out.println(subset);
        }
    }
}
