package daisie.algo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vklein on 08/09/15.
 */
public class Edge implements Comparable<Edge> {
    private String uid;
    /**
     * The source node of this link (if directed),
     * the first endpoint otherwise.
     */
    private Vertex source;
    /**
     * The destination node of this link (if directed),
     * the second endpoint otherwise.
     */
    private Vertex destination;

    private List<Vertex> vertices = new ArrayList<>();

    /**
     * @param source The source node of this link (if directed),the first endpoint otherwise.
     * @param destination The destination node of this link (if directed),the first endpoint otherwise.
     */
    public Edge(Vertex source, Vertex destination) {
        this.source = source;
        this.destination = destination;
        vertices.add(source);
        vertices.add(destination);
        this.uid = source.getUid() + "_" + destination.getUid();
    }

    public Vertex getSource() {
        return source;
    }

//    public void setSource(Vertex source) {
//        this.source = source;
//    }

    public Vertex getDestination() {
        return destination;
    }

//    public void setDestination(Vertex destination) {
//        this.destination = destination;
//    }

    public String getUid() {
        return uid;
    }

    /*
        On définit le cout de déplacement sur une arete comme étant sa distance
     */
    public Double cost() {
        return this.source.distance(this.destination);
    }

    @Override
    public int compareTo(Edge o) {
        return cost().compareTo(o.cost());
    }

    public static double getCost(List<Edge> edgeList) {
        double res =0;
        for(Edge edge: edgeList) {
            res += edge.cost();
        }
        return res;
    }

    public List<Vertex> getVertices() {
        return vertices;
    }
}
