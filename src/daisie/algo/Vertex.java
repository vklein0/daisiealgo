package daisie.algo;

/**
 * Created by vklein on 08/09/15.
 * 1 vertex est défini comme suit : idvertex,abscisse, ordonnée
 * Le cout de déplacement entre deux noeud est simplement
 * est simplement la distance entre entre leur 2 points
 */
public class Vertex {
    private long uid;
    private double x;
    private double y;

    public Vertex(long uid, double x, double y) {
        this.uid = uid;
        this.x = x;
        this.y = y;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    /**
     * retourne la distance entre deux sommets.
     * @param other
     * @return
     */
    public double distance(Vertex other) {
        return Math.sqrt(Math.pow(other.getX() - this.getX(), 2.0) + Math.pow(other.getY() - this.getY(), 2.0));
    }

    @Override
    public String toString() {
        return ""+uid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vertex vertex = (Vertex) o;

        if (uid != vertex.uid) return false;
        if (Double.compare(vertex.x, x) != 0) return false;
        return Double.compare(vertex.y, y) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (uid ^ (uid >>> 32));
        temp = Double.doubleToLongBits(x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
