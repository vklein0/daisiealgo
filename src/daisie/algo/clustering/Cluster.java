package daisie.algo.clustering;

import daisie.algo.Vertex;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vklein on 15/09/15.
 */
public class Cluster {
    private List<Vertex> listVertex;
    private Point2D mean;
    private Color color;

    public Cluster() {
        this(new ArrayList<Vertex>());
    }

    public Cluster(List<Vertex> listVertex) {
        this.listVertex = listVertex;
        mean = new Point();
    }

    public List<Vertex> getListVertex() {
        return listVertex;
    }

//
//    public void setListVertex(List<Vertex> listVertex) {
//        this.listVertex = listVertex;
//    }
    public boolean addVertex(Vertex vertex) {
        return this.listVertex.add(vertex);
    }

    public boolean removeVertex(Vertex vertex) {
        return this.listVertex.remove(vertex);
    }

    /**
     * Calcule le point moyen des points du cluster
     */
    public void computeMean() {
        double totX = 0;
        double totY = 0;

        for(Vertex vertex : listVertex) {
            totX += vertex.getX();
            totY += vertex.getY();
        }
        int size = this.listVertex.size();

        mean.setLocation(totX/size, totY/size);
    }

    /**
     * Retourne la distance avec le point moyen du cluster
     */
    public double distance(Vertex vertex) {
        return mean.distance(vertex.getX(), vertex.getY());
    }

    public void clear() {
        this.listVertex.clear();
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
