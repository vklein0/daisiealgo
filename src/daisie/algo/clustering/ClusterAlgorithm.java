package daisie.algo.clustering;

import daisie.algo.Vertex;

import java.util.List;

/**
 * Created by vklein on 15/09/15.
 */
public interface ClusterAlgorithm {

    /**
     * Return clusters of vertex (each List<Vertex> return is a cluster)
     * @param listVertex
     * @return
     */
    List<Cluster> doCluster(List<Vertex> listVertex, int nbCluster) throws Exception;
}
