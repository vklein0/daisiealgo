package daisie.algo.clustering;

import daisie.algo.Vertex;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vklein on 15/09/15.
 * Algo used to group data into K clusters
 * Domain : Vector quantization
 * ref history : Kmeans standard algorithm by Stuart Lloyd 1957.
 */
public class KMeansAlgo implements ClusterAlgorithm {

    private final double DISTORTION_TRESHOLD = 0.0001;
    private double distortion = 1;

    /**
     * Return clusters of vertex using kmean algorithm (each List<Vertex> return is a cluster)
     * @param listVertex
     * @return
     */
    @Override
    public List<Cluster> doCluster(List<Vertex> listVertex, int nbCluster) throws Exception {
        distortion = 1;
        if(nbCluster > listVertex.size())
            throw new Exception("listVertex.size() < nbCluster");

        //clusters initialisation
        List<Cluster> listCluster = new ArrayList<Cluster>();
        for(int i=0; i<nbCluster; i++) {
            Cluster cluster = new Cluster();
            listCluster.add(cluster);

            /*We affect a single different vertex for each cluster
             and compute the initial mean
            i.e. "select K initial cluster means".*/
            cluster.addVertex(listVertex.get(i));
            cluster.computeMean();
        }


        Cluster closestCluster;
        double distanceMin;

//        do {
        for(int i =0; i<300;i++) {
            for (Cluster cluster : listCluster) {
                cluster.clear();
            }

            for (Vertex vertex : listVertex) {
                closestCluster = null;
                distanceMin = Double.MAX_VALUE;

                for (Cluster cluster : listCluster) {
                    double tmpdist = cluster.distance(vertex);
                    if (tmpdist < distanceMin) {
                        distanceMin = tmpdist;
                        closestCluster = cluster;
                    }
                }
                closestCluster.addVertex(vertex);
            }

            for (Cluster cluster : listCluster) {
                cluster.computeMean();
            }
        }
//        } while(!distortionStable(listCluster));

        return listCluster;
    }



    private boolean distortionStable(List<Cluster> clusterList) {
        boolean res = false;
        double newDistortion = computeTotalDistortion(clusterList);
        System.out.println("new : " + newDistortion + " distortion : " + distortion + " " + (1- (newDistortion/distortion)));
        if(newDistortion - distortion != 0) {
            if (distortion == 1)
                res = false;
            else
                res = (1 - newDistortion / distortion) < DISTORTION_TRESHOLD;
            distortion = newDistortion;
        } else {
            res = true;
        }
        return res;
    }


    private double computeTotalDistortion(List<Cluster> clusterList) {
        double totalDistortion = 0;
        for(Cluster cluster: clusterList) {
            for(Vertex vertex : cluster.getListVertex()) {
                totalDistortion += Math.pow(cluster.distance(vertex), 2);
            }
        }
        System.out.println(" cpt tot dist " + totalDistortion);
        return totalDistortion;
    }
}
