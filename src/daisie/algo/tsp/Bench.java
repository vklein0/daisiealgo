package daisie.algo.tsp;

import daisie.algo.Edge;
import daisie.algo.Vertex;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by vklein on 06/10/15.
 * fire a series of tsp to compute average ratio of 2apx (Eularian tour shortcut)
 * and random insertion algorithm. The optimum is computed via linear optimisation.
 */
public class Bench {
    static final int NB_TIRS = 10000;
    static final int NB_VERTICES = 12;

    static final int WIDTH = 1000;
    static final int HEIGTH = 1000;


    public static SimpleDateFormat sdf = new SimpleDateFormat(" dd-MM-yy:HH:mm:ss");
    static DecimalFormat df = new DecimalFormat();
    static {
        df.setMaximumFractionDigits(3);
    }

    public static void main(String[] args) {
        List<BenchResult> benchResults = new ArrayList<>(NB_TIRS);
        List<Vertex> vertexList;
        List<Edge> el2Apx, elRi, elPl;

        //List of set with a ratio > 2
        List<BenchVerticesSet> verticesSetList = new ArrayList<>();

        double ratio2Apx, ratioRi;
        double cost2Apx, costRi, costPl;
        long computeTime2Apx =0;
        long computeTimeRi = 0;
        long computeTimePl = 0;
        long time2Apx, timeRi, timePl;
        long startTime;

        //initialize Tsp solvers
        TspSolver twoApxSolver = new TspTwoApprox4Metric();
        TspSolver randomInsertionSolver = new TspRandomInsertion();
        TspSolver lpSolver = new TspLP();


        Random random = new Random();
        int x,y;
        BenchResult result;
        for(int i=0; i<NB_TIRS; i++) {
            //pour chaque tirs on génère aléatoirement la carte des sommets
            vertexList = new ArrayList<>();

            for(int j=0; j<NB_VERTICES; j++) {
                x = random.nextInt(WIDTH);
                y = random.nextInt(HEIGTH);
                vertexList.add(new Vertex(j,x,y));
            }

            startTime   = System.currentTimeMillis();
            cost2Apx    = Edge.getCost(twoApxSolver.solveTSP(vertexList));
            time2Apx    = System.currentTimeMillis() - startTime;

            startTime   = System.currentTimeMillis();
            costRi      = Edge.getCost(randomInsertionSolver.solveTSP(vertexList));
            timeRi      = System.currentTimeMillis() - startTime;

            startTime   = System.currentTimeMillis();
            List<Edge> edgeListpl = lpSolver.solveTSP(vertexList);
            timePl      = System.currentTimeMillis() - startTime;
            costPl      = Edge.getCost(edgeListpl);

            if(isTour(edgeListpl, vertexList)) { //On filtre les resultats incomplets de LPSOLVER
                computeTime2Apx += time2Apx;
                computeTimeRi += timeRi;
                computeTimePl += timePl;

                result = new BenchResult(cost2Apx, costRi, costPl, time2Apx, timeRi, timePl);
                benchResults.add(result);
            } else {
                verticesSetList.add(new BenchVerticesSet(vertexList, cost2Apx, costRi, costPl));
            }

        }

        double rt2apxSum = 0;
        double rtRiSum = 0;

        for(BenchResult benchResult : benchResults) {
            rt2apxSum += benchResult.getRatio2Apx();
            rtRiSum += benchResult.getRatioRi();
        }
        StringBuilder sBuild = new StringBuilder();
        sBuild.append(benchResults.size()).append(" tirs et ").append(NB_VERTICES).append(" sommets");
        sBuild.append("\nTime apx ").append(df.format(computeTime2Apx/1000.0)).append(" s ");
        sBuild.append("\nTime random insertion ").append(df.format(computeTimeRi/1000.0)).append(" s ");
        sBuild.append("\nTime linear optimisation ").append(df.format(computeTimePl/1000.0)).append(" s ");
        sBuild.append("\nRatio 2 Apx  : " + df.format(rt2apxSum / benchResults.size()));
        sBuild.append("\nRatio RI  : " + df.format(rtRiSum / benchResults.size()));
        System.out.println(sBuild);
        writeResult(sBuild, benchResults);
        if(!verticesSetList.isEmpty())
            writeOptimumSup2(verticesSetList);
    }

    /**
     * Save the bench results
     * @param header
     * @param benchResults
     */
    private static void writeResult(StringBuilder header, List<BenchResult> benchResults) {
        String fileName = "V"+NB_VERTICES+"_T"+NB_TIRS+"_"+sdf.format(new Date());

        try {
            FileWriter writer = new FileWriter(fileName);
            writer.append(header);
            writer.append('\n');

            for(BenchResult result : benchResults) {
                writer.append(result.toString());
                writer.append('\n');
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeOptimumSup2(List<BenchVerticesSet> verticesSetList) {
        String fileName = "RATSUP2_V"+NB_VERTICES+"_T"+NB_TIRS+"_"+sdf.format(new Date());

        try {
            FileWriter writer = new FileWriter(fileName);
            for(BenchVerticesSet set: verticesSetList) {
                writer.append(set.toString());
                writer.append('\n');
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * return true if each vertex apear two times as an endpoint
     * in edgeList
     * @param edgeList
     * @param vertexList
     * @return
     */
    private static boolean isTour(List<Edge> edgeList, List<Vertex> vertexList) {
        boolean res = true;
        int cpt=0;
        if(edgeList.size() != vertexList.size())
            return false;
        for(Vertex vertex:vertexList) {
            cpt=0;
            for(Edge edge: edgeList) {
                if(edge.getVertices().contains(vertex))
                    cpt++;
            }
            if(cpt != 2) {
                res = false;
                break;
            }
        }
        return res;
    }
}
