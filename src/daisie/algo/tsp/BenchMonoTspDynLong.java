package daisie.algo.tsp;

import daisie.algo.Edge;
import daisie.algo.Vertex;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by vklein on 23/10/15.
 */
public class BenchMonoTspDynLong {
    static final int NB_TIRS = 1;
    static final int NB_EVENEMENTS = 2000000;
    static final int INITIAL_NBNODES = 50;
    static final int MAX_NBNODES = 100;
    static final int MIN_NBNODES = 5;

    static final int HEIGTH = 1000;
    static final int WIDTH = 1000;
    static Random random = new Random();
    static TspRandomInsertion solverRi = new TspRandomInsertion();
    static FileWriter writer = null;

    public static void main(String[] args) throws IOException {
        String fileName = "MonoTspDynLong_E"+NB_EVENEMENTS+"_MXNODE100_"+Bench.sdf.format(new Date());
        writer = new FileWriter(fileName);

        for(int i=0; i<NB_TIRS; i++) {
            List<BenchResult> results = runTest();
            writeResult(results);
        }
        writer.close();
    }

    public static List<BenchResult> runTest() {
        List<BenchResult> results = new ArrayList<>();
        double costRi, costPl;
        //init TSP
        List<Vertex> vertexList = new ArrayList<>();


        for(int j=0; j<INITIAL_NBNODES; j++) {
            vertexList.add(randomVertex(j));
        }

        List<Edge> edgeList = solverRi.solveTSP(vertexList);

        costRi = Edge.getCost(edgeList);
        results.add(new BenchResult(costRi, INITIAL_NBNODES));

        for(int i=0; i<NB_EVENEMENTS; i++) {
            boolean add=true;
            if(edgeList.size() == MAX_NBNODES)
                add = false;
            else if(edgeList.size() > MIN_NBNODES ) {
                add = random.nextBoolean();
            }

            if(add) {
                //Ajout
                edgeList = solverRi.solveTsp(edgeList, randomVertex(i+INITIAL_NBNODES));
            } else {
                //Retrait
                Vertex vertex = vertexList.get(random.nextInt(vertexList.size()));
                edgeList = solverRi.removeVertex(edgeList, vertex);
            }

            vertexList = TspHelper.getVertices(edgeList);

            costRi = Edge.getCost(edgeList);
            results.add(new BenchResult(costRi,  edgeList.size()));
        }
        return results;
    }

    private static Vertex randomVertex(int uid) {
        int x,y;
        x = random.nextInt(WIDTH);
        y = random.nextInt(HEIGTH);
        return new Vertex(uid, x, y);
    }

    private static void writeResult(List<BenchResult> results) {

        try {
            for(BenchResult result : results) {
                writer.append(result.getCostRi()+";"+result.getNbNode());
                writer.append('\n');
            }
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
