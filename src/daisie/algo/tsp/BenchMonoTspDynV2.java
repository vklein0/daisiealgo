package daisie.algo.tsp;

import daisie.algo.Edge;
import daisie.algo.Vertex;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by vklein on 15/10/15.
 * Le but de cette campagne de test est d'observer l'évolution
 * du ratio par rapport à l'optimum suivant le nombre d'évènement dynamique.
 * On part d'un tsp à 7 node résolu. Un évènement dynamique consiste soit
 * à ajouter un sommet (l'ajout dans la solution TSP est règlée comme une Random Insertion),
 * soit à retirer un sommet dans ce dernier cas on retire du tour les deux aretes du sommet retiré,
 * et l'on ajoute au tour l'arete reliant les deux sommets auxquels appartenaient les deux arêtes
 * précédement retirées.
 * V2. Tous les résultats dans le même fichier.
 */
public class BenchMonoTspDynV2 {

    static final int NB_TIRS = 1;
    static final int NB_EVENEMENTS = 1000000;
    static final int INITIAL_NBNODES = 10;
    static final int PERIODE = 1000;
    static final int MAX_NBNODES = 14;
    static final int MIN_NBNODES = 5;

    static final int HEIGTH = 1000;
    static final int WIDTH = 1000;
    static Random random = new Random();
    static TspRandomInsertion solverRi = new TspRandomInsertion();
    static TspSolver solverLP = new TspLP();
    static FileWriter writer = null;

    public static void main(String[] args) throws IOException {
        String fileName = "MonoTspDynGLPK_E"+NB_EVENEMENTS+"_P"+PERIODE+"_T"+NB_TIRS+"_"+Bench.sdf.format(new Date());
        writer = new FileWriter(fileName);

        for(int i=0; i<NB_TIRS; i++) {
            List<BenchResult> results = runTest();
            writeResult(results);
        }
        writer.close();
    }

    private static void writeResult(List<BenchResult> results) {

        try {
            writer.append("#BEGIN_RUN\n");
            for(BenchResult result : results) {
                writer.append(result.getCostRi()+";"+result.getCostPl()+";"+result.getRatioRi()+";"+result.getNbNode());
                writer.append('\n');
            }
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<BenchResult> runTest() {
        List<BenchResult> results = new ArrayList<>();
        double costRi, costPl;
        //init TSP
        List<Vertex> vertexList = new ArrayList<>();


        for(int j=0; j<INITIAL_NBNODES; j++) {
            vertexList.add(randomVertex(j));
        }

        List<Edge> edgeList = solverRi.solveTSP(vertexList);

        costRi = Edge.getCost(edgeList);
        costPl = Edge.getCost(solverLP.solveTSP(vertexList));
        results.add(new BenchResult(costRi, costPl, INITIAL_NBNODES));

        for(int i=0; i<NB_EVENEMENTS; i++) {
            boolean add=true;
            if(edgeList.size() == MAX_NBNODES)
                add = false;
            else if(edgeList.size() > MIN_NBNODES ) {
                add = random.nextBoolean();
            }

            if(add) {
                //Ajout
                edgeList = solverRi.solveTsp(edgeList, randomVertex(i+INITIAL_NBNODES));
            } else {
                //Retrait
                Vertex vertex = vertexList.get(random.nextInt(vertexList.size()));
                edgeList = solverRi.removeVertex(edgeList, vertex);
            }

            vertexList = TspHelper.getVertices(edgeList);

            if(i % PERIODE == 0) {
                costRi = Edge.getCost(edgeList);
                costPl = Edge.getCost(solverLP.solveTSP(vertexList));
                results.add(new BenchResult(costRi, costPl, edgeList.size()));
            }
        }
        return results;
    }

    private static Vertex randomVertex(int uid) {
        int x,y;
        x = random.nextInt(WIDTH);
        y = random.nextInt(HEIGTH);
        return new Vertex(uid, x, y);
    }
}
