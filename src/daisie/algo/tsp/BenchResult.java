package daisie.algo.tsp;

import java.text.DecimalFormat;

/**
 * Created by vklein on 06/10/15.
 */
public class BenchResult {
    static DecimalFormat df = new DecimalFormat();
    static {
        df.setMaximumFractionDigits(3);
    }

    private double cost2Apx;
    private double costRi;
    private double costPl;
    private double ratio2Apx;
    private double ratioRi;
    private long duration2Apx;
    private long durationRi;
    private long durationPl;
    private int nbNode;

    public BenchResult(double costRi, int nbNode) {
        this.costRi     = costRi;
        this.nbNode     = nbNode;
    }
    public BenchResult(double costRi, double costPl, int nbNode) {
        this.costRi         = costRi;
        this.costPl         = costPl;
        this.ratioRi        = costRi / costPl;
        this.nbNode         = nbNode;
    }

    public BenchResult(double cost2Apx, double costRi, double costPl, long duration2Apx, long durationRi, long durationPl) {
        this.cost2Apx       = cost2Apx;
        this.costRi         = costRi;
        this.costPl         = costPl;
        this.ratio2Apx      = cost2Apx / costPl;
        this.ratioRi        = costRi / costPl;
        this.duration2Apx   = duration2Apx;
        this.durationRi     = durationRi;
        this.durationPl     = durationPl;
    }

    public double getCost2Apx() {
        return cost2Apx;
    }

    public double getCostPl() {
        return costPl;
    }

    public double getCostRi() {
        return costRi;
    }

    public double getRatio2Apx() {
        return ratio2Apx;
    }

    public double getRatioRi() {
        return ratioRi;
    }

    public int getNbNode() {
        return nbNode;
    }

    @Override
    public String toString() {
        return cost2Apx+";"+duration2Apx+";"+ratio2Apx+";"+costRi+";"+durationRi+";"+ratioRi+";"+costPl+";"+durationPl;
    }
}
