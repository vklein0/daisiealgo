package daisie.algo.tsp;

import daisie.algo.Vertex;

import java.util.List;

/**
 * Created by vklein on 09/10/15.
 * Contient une liste de sommets anisi
 * que les distances mesurées pour les algos stp
 */
public class BenchVerticesSet {
    private List<Vertex> vertexList;
    private double cost2Apx, costRi, costPl;

    public BenchVerticesSet(List<Vertex> vertexList, double cost2Apx, double costRi, double costPl) {
        this.cost2Apx = cost2Apx;
        this.costPl = costPl;
        this.costRi = costRi;
        this.vertexList = vertexList;
    }

    public double getCost2Apx() {
        return cost2Apx;
    }

    public double getCostPl() {
        return costPl;
    }

    public double getCostRi() {
        return costRi;
    }

    public List<Vertex> getVertexList() {
        return vertexList;
    }

    @Override
    public String toString() {
        StringBuilder sBuild = new StringBuilder();
        sBuild.append(cost2Apx).append(';').append(costRi).append(';').append(costPl).append('[');
        for(Vertex vertex : vertexList) {
            sBuild.append('(').append(vertex.getX()).append(',').append(vertex.getY()).append(')');
        }
        sBuild.append(']');

        return sBuild.toString();
    }
}
