package daisie.algo.tsp;

import daisie.algo.Edge;
import daisie.algo.Vertex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by vklein on 14/09/15.
 */
public class TspChristofides implements TspSolver {
    @Override
    public List<Edge> solveTSP(List<Vertex> lVertex) {
        List<Edge> res = new ArrayList<>();
        List<Edge> lEdge = TspHelper.generateEdgeList(lVertex);

        //Calculate the minimum spanning tree T
        List<Edge> lEmst = TspHelper.getMst(lEdge, lVertex);

        //Calculate V' the set of vertices with odd degree in T
        List<Vertex> listOddVertices = findOddDegreeVertices(lEmst);

        //Calculate the minimal perfect matching M from V'

        //Unite matching and spanning tree T U M

        //Calculate Euler tour

        //Calculate shorcut tour
        // 1. Ordre de premiere aparition de chaque sommet
        List<Vertex> lOrdreApparition = new ArrayList<>();

        /*for(Edge edge: eulerianTour) {
            Vertex vertex = edge.getSource();
            if(!lOrdreApparition.contains(vertex)) {
                lOrdreApparition.add(vertex);
            }
        }*/

        return null;
    }

    private List<Edge> minimalWeightPerfectMatching(List<Vertex> oddVertices) {
        List<Edge> res = new ArrayList<>();
        double minimalWeight = Double.MAX_VALUE;

        return res;
    }
    /**
     * Return the list of odd degree vertices in the graph defined by lEdge
     * @param lEdge
     * @return
     */
    private List<Vertex> findOddDegreeVertices(List<Edge> lEdge) {
        List<Vertex> listOddVertices = new ArrayList<>();
        HashMap<Vertex, Integer> mapCount = new HashMap<>();
        for(Edge edge: lEdge) {
            processVertex(mapCount, edge.getSource());
            processVertex(mapCount, edge.getDestination());
        }

        for(Vertex vertex: mapCount.keySet()) {
            if((mapCount.get(vertex)%2) != 0)
                listOddVertices.add(vertex);
        }
        return listOddVertices;
    }

    private void processVertex(HashMap<Vertex,Integer> map, Vertex v) {
        if(map.keySet().contains(v)) {
            map.put(v,  map.get(v) + 1);
        } else {
            map.put(v, 1);
        }
    }
}
