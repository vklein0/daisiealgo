package daisie.algo.tsp;

import daisie.algo.Edge;
import daisie.algo.Vertex;
import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by vklein on 10/09/15.
 */
public class TspHelper {

    public static List<Edge> generateEdgeList(List<Vertex> lVertex) {
        ICombinatoricsVector<Vertex> initialSet = Factory.createVector(lVertex);
        Generator<Vertex> gen = Factory.createSubSetGenerator(initialSet);

        List<Edge> lEdge = new ArrayList<Edge>();
        gen = Factory.createSimpleCombinationGenerator(initialSet, 2);

        for(ICombinatoricsVector<Vertex> subset:gen) {
            lEdge.add(new Edge(subset.getValue(0), subset.getValue(1)));
//            System.out.println(subset);
        }
        return lEdge;
    }

    public static List<Vertex> getVertices(List<Edge> edgeList) {
        List<Vertex> vertexList = new ArrayList<>();
        for(Edge edge: edgeList) {
            for(Vertex vertex:edge.getVertices()) {
                if(!vertexList.contains(vertex)) {
                    vertexList.add(vertex);
                }
            }
        }
        return vertexList;
    }

    public static List<Edge> getMst(List<Edge> lEdge, List<Vertex> lVertex) {
        //Tri des aretes par coûts
        Collections.sort(lEdge);
        List<Edge> lEmst = new ArrayList<Edge>();
        List<Vertex> lVmst = new ArrayList<Vertex>();

        lVmst.add(lVertex.get(0));

        while(lVmst.size() != lVertex.size()) {
            boolean added=false;
            ListIterator<Edge> iterator = lEdge.listIterator();

            while(!added && iterator.hasNext()) {
                Edge edge = iterator.next();
                if(lVmst.contains(edge.getSource()) && !lVmst.contains(edge.getDestination())) {
                    lVmst.add(edge.getDestination());
                    lEmst.add(edge);
                    added = true;
                } else if(lVmst.contains(edge.getDestination()) && !lVmst.contains(edge.getSource())) {
                    lVmst.add(edge.getSource());
                    lEmst.add(edge);
                    added = true;
                }
            }
        }
        return lEmst;
    }
}
