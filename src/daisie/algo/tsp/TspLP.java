package daisie.algo.tsp;

import daisie.algo.Edge;
import daisie.algo.Vertex;
import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;
import scpsolver.lpsolver.LinearProgramSolver;
import scpsolver.lpsolver.SolverFactory;
import scpsolver.problems.LPSolution;
import scpsolver.problems.LPWizard;
import scpsolver.problems.LPWizardConstraint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by vklein on 08/09/15.
 * Classe de modélisation d'un exemple tsp avec ScpSolver
 */
public class TspLP implements TspSolver {

    public static void main(String[] args) {
        /*
        On définit temporairement une liste de daisie.algo.Vertex à la main
        Le but final est de sélectionner un fichier contenant le problème STP
        en entrée. Puis intégration avec jbotsim, les nodes dessinées représentent le
        problème.
         */
        List<Vertex> V = new ArrayList<>();
        V.add(new Vertex(0,0,0));
        V.add(new Vertex(1, 1, 3));
        V.add(new Vertex(2, 4, 3));
        V.add(new Vertex(3, 10, 3));
        V.add(new Vertex(4, 10, 15));
        V.add(new Vertex(5, 12, 3));
        V.add(new Vertex(6, 5, 13));
        V.add(new Vertex(7, 7, 7));
        V.add(new Vertex(8, 15, 7));
        new TspLP().solveTSP(V);
    }

    private static String vertexSetToString(List<Vertex> lVertex) {
        StringBuilder sb = new StringBuilder("(");
        for(Vertex v: lVertex) {
            sb.append(v.getUid());
            sb.append(" ");
        }
        sb.append(")");
        return sb.toString();
    }


    public List<Edge> solveTSP(List<Vertex> V) {

        /*Let S be a proper subset of V :: 0 C S C V
        soit lS la liste des S ou card(S) >= 3*/
        List<List<Vertex>> lS = new ArrayList<List<Vertex>>();

        ICombinatoricsVector<Vertex> initialSet = Factory.createVector(V);
        Generator<Vertex> gen = Factory.createSubSetGenerator(initialSet);

        for(ICombinatoricsVector<Vertex> subset:gen) {
            if(subset.getSize() >= 3 && subset.getSize() < V.size()) {
                lS.add(subset.getVector());
//                System.out.println(subset);
            }
        }

        /* Construction de la liste des arêtes, on est dans un graph complet */
        List<Edge> lEdge = TspHelper.generateEdgeList(V);

        LPWizard lpw = new LPWizard();
        //Construction de la fonction à minimiser
        int i;
        for(i =0 ; i < lEdge.size(); i++) {
            Edge edge = lEdge.get(i);
            lpw.plus(edge.getUid(), edge.cost());
        }

        /*
         Contrainte indiquant que tout noeud doit etre parcouru
         i.e. un arc entrant et un arc sortant,pas plus, pas moins.
        */
        for(Vertex v: V) {
            LPWizardConstraint constraint = lpw.addConstraint("c"+v.getUid(),2,"=");
            List<Edge> aretesAdjacentes = delta(new ArrayList<Vertex>(Arrays.asList(new Vertex[]{v})), lEdge);
            for(Edge edge : aretesAdjacentes) {
                constraint.plus(edge.getUid());
            }
        }

        /*sub-tour elimination constraints
        Pour tout les subset de V de cardinalité >= 3*/
        for(List<Vertex> subset : lS) {
            LPWizardConstraint constraint = lpw.addConstraint("st "+vertexSetToString(subset), 2, "<=");
            List<Edge> aretesAvecUnEndPoint = delta(subset, lEdge);
            for(Edge edge: aretesAvecUnEndPoint) {
                constraint.plus(edge.getUid());
            }
        }

        /*
        Les variables de présence d'arete ne peuvent valoir que 0 ou 1
        Ces variables sont actuellement identifiés par l'uid de l'arete
         */
        for(Edge edge : lEdge) {
            lpw.addConstraint("lb"+edge.getUid(), 0, "<=").plus(edge.getUid()).setAllVariablesInteger();
            lpw.addConstraint("ub"+edge.getUid(), 1, ">=").plus(edge.getUid()).setAllVariablesInteger();
        }

        LinearProgramSolver solver = SolverFactory.getSolver("LPSOLVE");
        lpw.setMinProblem(true);
        LPSolution lpSolution = lpw.solve(solver);

        List<Edge> res = new ArrayList<>();
        for(Edge edge : lEdge) {
            if(lpSolution.getBoolean(edge.getUid())) {
                res.add(edge);
            }
        }
        return res;
    }

    /**
     * Modéliser "d(S) C E set of edges that have on endpoint in S"
     * Retourne l'ensemble des aretes avec exactement une extrémité
     * dans le subset en paramètres
     * s = un subset strict de V
     */
    private List<Edge> delta(List<Vertex> s, List<Edge> lEdge) {
        List<Edge> res = new ArrayList<>();

        int cpt; //Compteur d'endpoint
        for(Edge e: lEdge) {
            cpt=0;
            for(Vertex v: s) {
                if(v.equals(e.getSource())) {
                    cpt++;
                } else if(v.equals(e.getDestination())) {
                    cpt++;
                }
            }
            if(cpt ==1)
                res.add(e);
        }
        return res;
    }
}
