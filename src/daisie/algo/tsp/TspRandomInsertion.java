package daisie.algo.tsp;

import daisie.algo.Edge;
import daisie.algo.Vertex;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vklein on 02/10/15.
 * randomly select a vertex
 * Each time: insert vertex at position that gives minimum increase of tour length
 */
public class TspRandomInsertion implements TspSolver {

    /* All possible edges (complete graph) for the current list
       of vertices
      */
    private List<Edge> allEdges;

    @Override
    public List<Edge> solveTSP(List<Vertex> vertexList) {
        List<Edge> edgeList  = new ArrayList<>();
        allEdges  = TspHelper.generateEdgeList(vertexList);

        Vertex cVertex = vertexList.get(0);
        Vertex tmpVertex = vertexList.get(1);

        Edge cEdge = new Edge(cVertex, tmpVertex);
        edgeList.add(cEdge);

        for(int i=2; i<vertexList.size(); i++) {
            cVertex = vertexList.get(i);

            //Mise à jour du chemin le plus court trouvé, avec le dernier sommet
            edgeList = minimalCostInsertion(edgeList, cVertex);
        }
        return edgeList;
    }

    /**
     * Ajoute le sommet en parametre à la liste d'arete en parametre
     * Le sommet est ajouté en supprimant l'une des aretes de la liste
     * et en ajoutant deux nouvelles pour relier le nouveau sommet.
     * L'arete supprimé est celle qui doit permettre d'avoir la somme des distance des
     * aretes du résultat la plus petite possible.
     * @param edgeList
     * @param vertex
     */
    public List<Edge> solveTsp(List<Edge> edgeList, Vertex vertex) {
        List<Vertex> vertexList = TspHelper.getVertices(edgeList);
        vertexList.add(vertex);
        allEdges = TspHelper.generateEdgeList(vertexList);
        return minimalCostInsertion(edgeList, vertex);
    }

    /**
     * Ajoute le sommet en parametre à la liste d'arete en parametre
     * Le sommet est ajouté en supprimant l'une des aretes de la liste
     * et en ajoutant deux nouvelles pour relier le nouveau sommet.
     * L'arete supprimé est celle qui doit permettre d'avoir la somme des distance des
     * aretes du résultats la plus petite possible.
     * @param edgeList
     * @param vertex
     */
    private List<Edge> minimalCostInsertion(List<Edge> edgeList, Vertex vertex) {


        //On cherche l'insertion de moindre cout.
        double minimalCost = Double.MAX_VALUE;
        List<Edge> testList;
        List<Edge> currentBestList = new ArrayList<>();

        for(Edge edge: edgeList) {
            testList = new ArrayList<>(edgeList);
            //On retire l'arete testé si l'on est au dela du 3eme sommet
            if(edgeList.size() > 2)
                testList.remove(edge);
            //On ajoute les deux aretes vers le nouveaux sommet
            testList.add(getEdge(vertex, edge.getSource()));
            testList.add(getEdge(vertex, edge.getDestination()));
            if(minimalCost > Edge.getCost(testList)) {
                currentBestList = testList;
                minimalCost = Edge.getCost(testList);
            }
        }
        return currentBestList;
    }

    private Edge getEdge(Vertex vOne, Vertex vTwo) {
        Edge res = null;
        List<Vertex> vertices;
        for(Edge edge : allEdges) {
            vertices = edge.getVertices();
            if(vertices.contains(vOne) && vertices.contains(vTwo)) {
                res = edge;
                break;
            }
        }
        return res;
    }

    /*
        Hors tsp retire un sommet du tour en paramètres, les 2 sommets qui étaient reliés
         au sommet retiré sont reliés entre eux pour compléter le tour.
     */
    public List<Edge> removeVertex(List<Edge> edgeList, Vertex vertex) {
        List<Edge> newTour = new ArrayList<>(edgeList);
        Vertex firstNeighbour = null;

        //On enleve les deux aretes contenant le sommet en parametres
        boolean trouve = false;
        Edge edge;
        for(int i=0; i<edgeList.size() && !trouve; i++) {
            edge = edgeList.get(i);
            if(edge.getVertices().contains(vertex)) {
                for(Vertex tmpVertex : edge.getVertices()) {
                    if(!tmpVertex.equals(vertex)) {
                        if(firstNeighbour == null) {
                            firstNeighbour = tmpVertex;
                        } else {
                            if(firstNeighbour.getUid() < tmpVertex.getUid())
                                newTour.add(new Edge(firstNeighbour, tmpVertex));
                            else
                                newTour.add(new Edge(tmpVertex, firstNeighbour));
                            trouve = true;
                        }
                    }
                }
                newTour.remove(edge);
            }
        }
        return newTour;
    }
}
