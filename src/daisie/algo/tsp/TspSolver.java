package daisie.algo.tsp;

import daisie.algo.Edge;
import daisie.algo.Vertex;

import java.util.List;

/**
 * Created by vklein on 09/09/15.
 */
public interface TspSolver {
    /**
     * @param V
     * @return HashMap<String, Boolean> :
     * La clef identifie l'arete au format idNoeud1_idNoeud2
     * Faux sinon.
     */
    List<Edge> solveTSP(List<Vertex> V);
}
