package daisie.algo.tsp;

import daisie.algo.Edge;
import daisie.algo.Vertex;

import java.util.*;

/**
 * Created by vklein on 09/09/15.
 */
public class TspTwoApprox4Metric implements TspSolver {


    @Override
    public List<Edge> solveTSP(List<Vertex> lVertex) {
        List<Edge> res = new ArrayList<>();
        List<Edge> lEdge = TspHelper.generateEdgeList(lVertex);

        //Find a MST
        List<Edge> lEmst = TspHelper.getMst(lEdge, lVertex);


        //Double every edge of the MST to obtain an Eulerian graph
        lEmst = doubleEveryEdge(lEmst);
        List<Edge> eulerianTour = findEulerianTour(lEmst, lVertex);

        //Contruction du shortcut Tour
        // 1. Ordre de premiere aparition de chaque sommet
        List<Vertex> lOrdreApparition = new ArrayList<>();

        for(Edge edge: eulerianTour) {
            Vertex vertex = edge.getSource();
            if(!lOrdreApparition.contains(vertex)) {
                lOrdreApparition.add(vertex);
            }
        }

        Vertex source = null;
        Vertex src=null,dest=null;

        for(Vertex vertex:lOrdreApparition) {
            if(source != null) {
                //On met les id dans l'ordre du + pti au + grand
                if(source.getUid() < vertex.getUid()) {
                    src = source;
                    dest = vertex;
                } else {
                    dest = source;
                    src = vertex;
                }
                res.add(new Edge(src,dest));
            }
            source = vertex;
        }
        //fermeture du tsp
        if(lOrdreApparition.size()>0) {
            Vertex vertex = lOrdreApparition.get(0);
            //On met les id dans l'ordre du + pti au + grand
            if(source.getUid() < vertex.getUid()) {
                src = source;
                dest = vertex;
            } else {
                dest = source;
                src = vertex;
            }
            res.add(new Edge(src,dest));
        }

        return res;
    }

    /**
     * Retourne un tour eulerien, suppose que le graphe en entrée comporte exactement
     * 2 aretes entre chaques sommets voisins (MST dont les aretes sont doublées).
     * @param lEulerian
     * @param lVertex
     * @return
     */
    private List<Edge> findEulerianTour(List<Edge> lEulerian, List<Vertex> lVertex) {
        /*
            Création d'une hashmap avec sommet en clef et la liste des aretes
            qui partent du sommet en valeur. Le but est uniquement d'avoir un accès pratique
            à toutes les aretes partantes d'un sommet
         */
        HashMap<Vertex, List<Edge>> mapEdgeList = new HashMap<>();
        for(Vertex v : lVertex) {
            List<Edge> lEdge = new ArrayList<>();

            for(Edge edge: lEulerian) {
                if(v.equals(edge.getSource())) {
                    lEdge.add(edge);
                }
            }
            mapEdgeList.put(v, lEdge);
        }

        //Liste représentant le tour eulerian début du tour en index 0 etc ...
        List<Edge> eulerianTour = new ArrayList<>();

        //Sommet courant.
        Vertex vCourant = lVertex.get(0);
        //Sommets déjà visités
        List<Vertex> lVertexVisited = new ArrayList<>();
        lVertexVisited.add(vCourant);
        //Aretes à visiter
        List<Edge> lEdgeToVisit = new ArrayList<>(lEulerian);

        while(lEdgeToVisit.size() >0) {
            List<Edge> lEdgeSrc = mapEdgeList.get(vCourant);
            Edge edge = null;

            for(int i=0; i<lEdgeSrc.size(); i++) {
                edge = lEdgeSrc.get(i);
                vCourant = edge.getDestination();
                //Si la destination n'as jamais été visitée
                //on sélectionne l'arete, sinon on sélectionne la derniere de la liste
                if (!lVertexVisited.contains(edge.getDestination())){
                    lVertexVisited.add(vCourant);
                    break;
                }
            }
            lEdgeSrc.remove(edge);
            lEdgeToVisit.remove(edge);
            eulerianTour.add(edge);
        }

        return eulerianTour;
    }

    private List<Edge> doubleEveryEdge(List<Edge> lEdge) {
        List<Edge> res = new ArrayList<>(lEdge);
        for(Edge edge: lEdge) {
            res.add(new Edge(edge.getDestination(), edge.getSource()));
        }
        return res;
    }

}
