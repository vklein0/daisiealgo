package jbotsim.poi;

import jbotsim.poi.ui.ClusterTspViewer;
import jbotsim.poi.ui.JClusterTspTopology;
import jbotsim.Topology;

/**
 * Created by vklein on 25/09/15.
 */
public class ClusteringAndSTPDemo {
    public static void main(String[] args) {
        Topology topology = new Topology(900,600);
        new ClusterTspViewer(new JClusterTspTopology(topology));
    }
}
