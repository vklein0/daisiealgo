package jbotsim.poi;

import daisie.algo.Vertex;
import jbotsim.Node;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by vklein on 01/10/15.
 */
public class DaisieAlgo2JbotsimHelper {

    private static DaisieAlgo2JbotsimHelper singleton = null;

    private DaisieAlgo2JbotsimHelper() {
    }

    public static DaisieAlgo2JbotsimHelper getSingleton() {
        if(singleton == null) {
            singleton = new DaisieAlgo2JbotsimHelper();
        }
        return singleton;
    }

    /**
     * map one jbotsim Node into one daisie algo Vertex
     * @param node
     * @return
     */
    public Vertex nodeToVertex(Node node) {
        return new Vertex(node.getID(), node.getX(), node.getY());
    }

    /**
     * map a jbotsim node collection into Vertex list
     * @param nodeCollection
     * @return
     */
    public List<Vertex> nodesToVertices(Collection<Node> nodeCollection) {
        List<Vertex> res = new ArrayList<>();
        for(Node node: nodeCollection) {
            res.add(nodeToVertex(node));
        }
        return res;
    }
}
