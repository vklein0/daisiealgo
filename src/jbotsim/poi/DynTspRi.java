package jbotsim.poi;

import jbotsim.Topology;
import jbotsim.poi.ui.ClusterTspViewer;
import jbotsim.poi.ui.JClusterTspTopology;
import jbotsim.poi.ui.JDynTspTopology;
import jbotsim.ui.JViewer;

/**
 * Created by vklein on 13/10/15.
 */
public class DynTspRi {

    public static void main(String[] args) {
        Topology topology = new Topology(900,600);
        new JViewer(new JDynTspTopology(topology));
    }
}
