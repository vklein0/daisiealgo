package jbotsim.poi;

import jbotsim.poi.ui.JTspTopology;
import jbotsim.ui.JViewer;

/**
 * Created by vklein on 09/09/15.
 */
public class TspSolve {

    public static void main(String[] args) {
        JTspTopology jTspTopology = new JTspTopology();
        new JViewer(jTspTopology);
    }
}
