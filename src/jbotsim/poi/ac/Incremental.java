package jbotsim.poi.ac;

import jbotsim.Link;
import jbotsim.Node;
import jbotsim.Topology;
import jbotsim.event.TopologyListener;
import jbotsim.ui.JViewer;

import java.util.List;

/**
 * Created by acasteig on 9/30/15.
 */
public class Incremental implements TopologyListener {

    Topology tp;

    public Incremental(Topology tp) {
        this.tp = tp;
        tp.addTopologyListener(this);
    }

    private double getActualCost(){
        double sum = 0;
        for (Link l : tp.getLinks())
            sum += l.getLength();
        return sum;
    }

    @Override
    public void onNodeAdded(Node node) {
        List<Node> nodes = tp.getNodes();
        if (nodes.size() <= 3) {
            for (Node n2 : nodes)
                if (n2 != node)
                    tp.addLink(new Link(node, n2));
        } else {
            double minDelta = Double.MAX_VALUE;
            Link candidate = null;
            for (Link l : tp.getLinks()) {
                double delta = node.distance(l.endpoint(0)) + node.distance(l.endpoint(1)) - l.getLength();
                if (delta < minDelta) {
                    candidate = l;
                    minDelta = delta;
                }
            }
            tp.addLink(new Link(node, candidate.endpoint(0)));
            tp.addLink(new Link(node, candidate.endpoint(1)));
            tp.removeLink(candidate);
        }
        System.out.println("incremental: " + getActualCost());
    }

    @Override
    public void onNodeRemoved(Node node) {
        Node n1=null, n2=null;
        for (Node nit : tp.getNodes()){
            if (nit.getLinks().size() == 1){
                if (n1 == null)
                    n1 = nit;
                else
                    n2 = nit;
            }
        }
        tp.addLink(new Link(n1, n2));
    }

    public static void main(String[] args) {
        Topology tp = new Topology();
        new JViewer(tp);
        tp.disableWireless();
        new Incremental(tp);
        new MirrorLP(tp);
        tp.addNode(100,100);
        tp.addNode(100,200);
        tp.addNode(200,100);
        tp.addNode(200,200);
    }
}
