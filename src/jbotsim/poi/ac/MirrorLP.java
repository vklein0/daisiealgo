package jbotsim.poi.ac;

import daisie.algo.Edge;
import daisie.algo.Vertex;
import daisie.algo.tsp.TspLP;
import daisie.algo.tsp.TspSolver;
import jbotsim.Link;
import jbotsim.Node;
import jbotsim.Topology;
import jbotsim.event.TopologyListener;
import jbotsim.poi.node.Drone;
import jbotsim.ui.JViewer;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by acasteig on 9/30/15.
 */
public class MirrorLP implements TopologyListener {

    Topology myTp;

    public MirrorLP(Topology tp) {
        tp.addTopologyListener(this);
        myTp = new Topology();
        myTp.setCommunicationRange(1000);
        (new JViewer(myTp)).getJTopology().setInteractive(false);
    }

    private void displayResultTsp(List<Edge> resultE) {
        List<String> result = new ArrayList<>();

        for(Edge edge: resultE) {
            result.add(edge.getUid());
        }
        for (Link link : myTp.getLinks()) {
            link.setWidth(0);
            link.setColor(Color.darkGray);
            String s1 = link.endpoint(0) + "_" + link.endpoint(1);
            if(result.contains(s1))
                link.setWidth(1);
        }
    }

    private List<Vertex> getListVertex(List<Node> lNode) {
        List<Vertex> lVertex = new ArrayList<>();
        for(Node node: lNode)
            if(node.getClass() != Drone.class)
                lVertex.add(new Vertex(node.getID(), node.getX(), node.getY()));
        return lVertex;
    }

    private double getOptimalCost(List<Edge> edges){
        double sum = 0;
        for (Edge e : edges)
            sum += e.cost();
        return sum;
    }

    @Override
    public void onNodeAdded(Node node) {
        myTp.addNode(node.getX(), node.getY());
        update();
    }

    @Override
    public void onNodeRemoved(Node node) {
        for (Node n2 : new ArrayList<Node>(myTp.getNodes()))
            if (n2.getLocation().equals(node.getLocation()))
                myTp.removeNode(n2);
        update();
    }

    private void update() {
        for (Link link : myTp.getLinks())
            link.setWidth(0);
        List<Node> nodes = myTp.getNodes();
        TspSolver solver = new TspLP();
        List<Edge> edges = solver.solveTSP(getListVertex(nodes));
        displayResultTsp(edges);
        System.out.println("optimal: " + getOptimalCost(edges)+"\n");
    }
}
