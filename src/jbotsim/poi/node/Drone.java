package jbotsim.poi.node;

import jbotsim.Link;
import jbotsim.Node;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by vklein on 29/09/15.
 */
public class Drone extends Node {
    //Distance traveled by mouvement
    public static final double DEFAULT_STEP = 1.5;
    public static final int COMMUNICATION_RANGE = 0;
    public static final int SENSING_RANGE = 0;

    private List<Link> linkList;
    private List<Node> pointList = new ArrayList<>();
    private Node waypoint;
    private double maxMove = DEFAULT_STEP;

    public Drone() {
        super();
        disableWireless();
        setCommunicationRange(COMMUNICATION_RANGE);
        setSensingRange(SENSING_RANGE);
        setIcon("/jbotsim/resources/uav.png");
        setSize(20);
    }

    @Override
    public void onClock() {
        if(waypoint != null) {
            double toDest  = distance(waypoint);
            if (toDest <= maxMove ) {
                this.move(toDest);
                //waypoint reached define new wp
                nextWayPoint();
            } else {
                this.move(maxMove);
            }
        }
    }

//    public void setWaypointList(List<Point2D> pointList) {
//        this.pointList = pointList;
//        nextWayPoint();
//    }

    private void nextWayPoint() {
        if(pointList.size() > 0) {
            if(waypoint == null) {
                waypoint = pointList.get(0);
            } else {
                int idx = pointList.indexOf(waypoint);
                if(idx+1 < pointList.size())
                    waypoint = pointList.get(idx+1);
                else //On recommence du debut de la liste
                    waypoint = pointList.get(0);
            }
            this.setDirection(waypoint.getLocation());
        }
    }

    public void setLinkList(List<Link> linkList) {
        if(linkList != null && linkList.size() >0) {
            pointList = new ArrayList<>();

            Node cNode = null;
            Node firstNode = null;
            Link cLink = null;

            //init
            cLink = linkList.get(0);
            pointList.add(cLink.source);
            firstNode = cLink.source;
            pointList.add(cLink.destination);
            cNode = cLink.destination;

            if(linkList.size() == 1)
                return;

            //Sinon on se trouve dans des "anneaux" ou nbNodes = nbLinks
            while(cNode != firstNode) {
                for(Link link: linkList) {
                    if(link != cLink && link.endpoints().contains(cNode)) {
                        //Suite du chemin
                        cLink = link;
                        for(Node node : link.endpoints()) {
                            if(node != cNode) {
                                cNode = node;
                                pointList.add(node);
                                break;
                            }
                        }
                    }
                }
            } // end while
            nextWayPoint();
        } // end setWaypointList
    }

    public void addNode(Node node) {

    }
}
