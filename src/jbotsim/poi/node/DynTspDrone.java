package jbotsim.poi.node;

import daisie.algo.Edge;
import daisie.algo.Vertex;
import daisie.algo.tsp.TspRandomInsertion;
import jbotsim.Link;
import jbotsim.Node;
import jbotsim.poi.DaisieAlgo2JbotsimHelper;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vklein on 13/10/15.
 */
public class DynTspDrone extends Node {

    //Distance traveled by mouvement
    public static final double DEFAULT_STEP = 1.5;
    public static final int COMMUNICATION_RANGE = 0;
    public static final int SENSING_RANGE = 0;

    DaisieAlgo2JbotsimHelper da2jHelper = DaisieAlgo2JbotsimHelper.getSingleton();

    private TspRandomInsertion tspSolver = new TspRandomInsertion();
    private List<Edge> edgeList;
    private List<Node> waypointList = new ArrayList<>();
    private Node waypoint;
    private double maxMove = DEFAULT_STEP;

    public DynTspDrone() {
        super();
        disableWireless();
        setCommunicationRange(COMMUNICATION_RANGE);
        setSensingRange(SENSING_RANGE);
        setIcon("/jbotsim/resources/uav.png");
        setSize(20);
    }

    public void addNodes(List<Node> nodeList) {
        //Call random Insertion tsp
        List<Vertex> vertexList = da2jHelper.nodesToVertices(nodeList);
        edgeList = tspSolver.solveTSP(vertexList);
        setWaypointList(getLinksTsp());

    }

    public void addNode(Node node) {
        Vertex vertex = da2jHelper.nodeToVertex(node);

        //On ajoute le sommet à la solution Tsp existante
        edgeList = tspSolver.solveTsp(edgeList, vertex);
        setWaypointList(getLinksTsp());
    }

    public void removeNode(Node node) {
        edgeList = tspSolver.removeVertex(edgeList, da2jHelper.nodeToVertex(node));
        setWaypointList(getLinksTsp());
    }

    private List<Link> getLinksTsp() {
        List<Link> linkList = new ArrayList<>();
        List<String> listUid = new ArrayList<>();
        for(Edge edge: edgeList) {
            listUid.add(edge.getUid());
        }

        for (Link link : getTopology().getLinks()) {
            String s = link.endpoint(0) + "_" + link.endpoint(1);
            if(listUid.contains(s)) {
                link.setColor(Color.GREEN);
                link.setWidth(3);
                linkList.add(link);
            }
        }
        return linkList;
    }

    private void setWaypointList(List<Link> linkList) {
        if(linkList != null && linkList.size() >0) {
            waypointList = new ArrayList<>();

            Node cNode = null;
            Node firstNode = null;
            Link cLink = null;

            //init
            cLink = linkList.get(0);
            waypointList.add(cLink.source);
            firstNode = cLink.source;
            waypointList.add(cLink.destination);
            cNode = cLink.destination;

            if(linkList.size() == 1)
                return;

            //Sinon on se trouve dans des "anneaux" ou nbNodes = nbLinks
            while(cNode != firstNode) {
                for(Link link: linkList) {
                    if(link != cLink && link.endpoints().contains(cNode)) {
                        //Suite du chemin
                        cLink = link;
                        for(Node node : link.endpoints()) {
                            if(node != cNode) {
                                cNode = node;
                                waypointList.add(node);
                                break;
                            }
                        }
                    }
                }
            } // end while
            nextWayPoint();
        } // end setWaypointList
    }

    //Set the next way point
    private void nextWayPoint() {
        if(waypointList.size() > 0) {
            if(waypoint == null) {
                waypoint = waypointList.get(0);
            } else {
                int idx = waypointList.indexOf(waypoint);
                if(idx+1 < waypointList.size())
                    waypoint = waypointList.get(idx+1);
                else //On recommence du debut de la liste
                    waypoint = waypointList.get(0);
            }
            this.setDirection(waypoint.getLocation());
        }
    }

    @Override
    public void onClock() {
        if(waypoint != null) {
            double toDest  = distance(waypoint);
            if (toDest <= maxMove ) {
                this.move(toDest);
                //waypoint reached define new wp
                nextWayPoint();
            } else {
                this.move(maxMove);
            }
        }
    }

    public void stop() {
        waypoint = null;
    }
}
