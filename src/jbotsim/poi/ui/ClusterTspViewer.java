package jbotsim.poi.ui;

import jbotsim.Topology;
import jbotsim.ui.JTopology;
import jbotsim.ui.JViewer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by vklein on 25/09/15.
 */
public class ClusterTspViewer extends JViewer {

    private static int DEFAULT_NB_AGENT = 1;
    private int nbAgent = DEFAULT_NB_AGENT;
    protected JClusterTspTopology jctt;



    public ClusterTspViewer(JTopology jTopology) {
        this(jTopology, true);
    }

    public ClusterTspViewer(Topology topology) {
        this(new JClusterTspTopology(topology), true);
    }

    public ClusterTspViewer(Topology topology, boolean windowed) {
        this(new JClusterTspTopology(topology), windowed);
    }

    public ClusterTspViewer(JTopology jTopology, boolean windowed) {
        super(jTopology, windowed);
        jctt = (JClusterTspTopology) jTopology;
        callUpdateAgents();
        if(windowed) {
            this.window.remove(this.jtp);
            this.window.setLayout(new BoxLayout(window.getContentPane(), BoxLayout.Y_AXIS));
            this.window.add(this.jtp);
            this.window.add(new ClusterTspPanel());
            this.window.pack();
        }
    }

    private void callUpdateAgents() {
        jctt.setNbAgents(nbAgent);
        jctt.updateAgents();
    }

    private class ClusterTspPanel extends JPanel implements ActionListener {

        public static final String LABEL_NOMBRE_AGENTS = "Nombre d'agents : ";
        public static final String CMD_ADD = "add";
        public static final String CMD_SUB = "sub";
        public static final String CMD_COMPUTE = "compute";
        public static final String CMD_TSP = "tsp";
        public static final int MAX_NB_AGENTS = 20;
        public static final int MIN_NB_AGENTS = 1;


        private JLabel jLabelAgt;
        private JRadioButton rb2Approx;
        private JRadioButton rbRandomInsertion;
        private JRadioButton rbLinearProg;
        private ButtonGroup buttonGroup;

        public ClusterTspPanel() {
            JPanel panAlgoChoice  = new JPanel();
            JPanel panButton      = new JPanel();
            this.setLayout(new FlowLayout(FlowLayout.CENTER));

            /*********************************************
                A Choix de l'algorithme de TSP
             *********************************************/
            panAlgoChoice.setLayout(new BoxLayout(panAlgoChoice, BoxLayout.Y_AXIS));
            
            buttonGroup         = new ButtonGroup();
            rb2Approx           = new JRadioButton("Algo 2 approx");
            rbRandomInsertion   = new JRadioButton("Random insertion");
            rbLinearProg        = new JRadioButton("Optimisation linéaire");

            buttonGroup.add(rb2Approx);
            buttonGroup.add(rbRandomInsertion);
            buttonGroup.add(rbLinearProg);

            panAlgoChoice.add(rb2Approx);
            panAlgoChoice.add(rbRandomInsertion);
            panAlgoChoice.add(rbLinearProg);

            //2 Approx par défaut
            rb2Approx.setSelected(true);

            /*********************************************
             A Boutons règlage du nombre d'agents
             *********************************************/

            jLabelAgt           = new JLabel(LABEL_NOMBRE_AGENTS+nbAgent);
            JButton bAdd        = new JButton();
            JButton bSub        = new JButton();
            JButton bCompute    = new JButton("Cluster");
            JButton bTsp        = new JButton("Tsp");

            bAdd.setActionCommand(CMD_ADD);
            bAdd.addActionListener(this);
            bAdd.setIcon(new ImageIcon("src/jbotsim/resources/plus65.png"));

            bSub.setActionCommand(CMD_SUB);
            bSub.addActionListener(this);
            bSub.setIcon(new ImageIcon("src/jbotsim/resources/moins65.png"));

            bCompute.setActionCommand(CMD_COMPUTE);
            bCompute.addActionListener(this);
            bCompute.setSize(bCompute.getWidth(), bSub.getHeight());

            bTsp.setActionCommand(CMD_TSP);
            bTsp.addActionListener(this);

            panButton.setLayout(new FlowLayout(FlowLayout.RIGHT));
            panButton.add(jLabelAgt);
            panButton.add(bSub);
            panButton.add(bAdd);
            panButton.add(bCompute);
            panButton.add(bTsp);

            this.add(panButton);
            this.add(panAlgoChoice);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            String actionCommand = e.getActionCommand();

            if(CMD_ADD.equals(actionCommand)) {
                if(nbAgent < MAX_NB_AGENTS) {
                    nbAgent++;
                    callUpdateAgents();
                }
                jLabelAgt.setText(LABEL_NOMBRE_AGENTS+nbAgent);
            } else if(CMD_SUB.equals(actionCommand)) {
                if(nbAgent > MIN_NB_AGENTS) {
                    nbAgent--;
                    callUpdateAgents();
                }
                jLabelAgt.setText(LABEL_NOMBRE_AGENTS+nbAgent);
            } else if(CMD_COMPUTE.equals(actionCommand)) {
                jctt.performKeans(nbAgent);
            } else if(CMD_TSP.equals(actionCommand)) {
                if(rb2Approx.isSelected()) {
                    jctt.callTsp2ApxOnClusters();
                } else if(rbLinearProg.isSelected()) {
                    jctt.callTspLPOnClusters();
                } else if(rbRandomInsertion.isSelected()) {
                    jctt.callTspRIOnClusters();
                }
            }
        }
    }
}
