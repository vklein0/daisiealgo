package jbotsim.poi.ui;

import daisie.algo.Edge;
import daisie.algo.Vertex;
import daisie.algo.clustering.Cluster;
import daisie.algo.clustering.KMeansAlgo;
import daisie.algo.tsp.TspHelper;
import daisie.algo.tsp.TspRandomInsertion;
import jbotsim.poi.DaisieAlgo2JbotsimHelper;
import jbotsim.poi.node.Drone;
import daisie.algo.tsp.TspLP;
import daisie.algo.tsp.TspTwoApprox4Metric;
import jbotsim.Link;
import jbotsim.Node;
import jbotsim.Topology;
import jbotsim.event.ConnectivityListener;
import jbotsim.ui.CommandListener;
import jbotsim.ui.JTopology;
import jbotsim.utils.ColorUtils;

import java.awt.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by vklein on 29/09/15.
 */
public class JClusterTspTopology extends JTopology implements ConnectivityListener, CommandListener {

    //Constantes
    private static final String CMD_CLEAR_NODES             = "Clear all nodes";
    private static final String CMD_ENABLE_DISABLE_DRONE    = "Enable/Disable Drone";
    private static final String CMD_COMPUTE_RATIO           = "Compute ratios";
    private static DaisieAlgo2JbotsimHelper d2AHelper       = DaisieAlgo2JbotsimHelper.getSingleton();
    private static DecimalFormat df = new DecimalFormat();
    static {
        df.setMaximumFractionDigits(3);
    }

    private TspLP solverLP;
    private TspTwoApprox4Metric solver2A;
    private TspRandomInsertion solverRI;
    private KMeansAlgo kMeansAlgo;
    private HashMap<Long, Node> mapIdNodes;
    private List<Cluster> listCluster = new ArrayList<>();
    private List<Drone> listDrone = new ArrayList<>();
    private List<List<Link>> listClusterLink = new ArrayList<>();
    private boolean droneEnabled = false;

    private int nbAgents = 3;

    public JClusterTspTopology() {
        this(new Topology());
    }

    public JClusterTspTopology(Topology topology) {
        super(topology);
        topology.setCommunicationRange(1000);
        topology.addConnectivityListener(this);
        addCommandListener(this);

        //Commands
        addCommand(CMD_CLEAR_NODES);
        addCommand(CMD_ENABLE_DISABLE_DRONE);
        addCommand(CMD_COMPUTE_RATIO);
        initTspSolver();
        initNodes();
    }

    private void initTspSolver() {
        solverLP = new TspLP();
        solver2A = new TspTwoApprox4Metric();
        solverRI = new TspRandomInsertion();
        kMeansAlgo = new KMeansAlgo();
    }

    private void initNodes() {
        /*(324.0,48.0)(499.0,298.0)(10.0,324.0)(849.0,110.0)(782.0,419.0)(839.0,741.0)(950.0,608.0)(523.0,261.0)(248.0,24.0)(853.0,490.0)*/
//        this.topo.addNode(324,48,new Node());
//        this.topo.addNode(499.0,298.0,new Node());
//        this.topo.addNode(10.0,324.0,new Node());
//        this.topo.addNode(849.0,110.0,new Node());
//        this.topo.addNode(782.0,419.0,new Node());
//        this.topo.addNode(839.0,741.0,new Node());
//        this.topo.addNode(950,608,new Node());
//        this.topo.addNode(523,261,new Node());
//        this.topo.addNode(248,24,new Node());
//        this.topo.addNode(853,490,new Node());
    }

    public void callTsp2ApxOnClusters() {
        hideLinks();
        listClusterLink.clear();
        List<Link> linkList;
        for(Cluster cluster: listCluster) {
            linkList = callTsp2A(cluster.getListVertex(), cluster.getColor());
            listClusterLink.add(linkList);
        }
        if(droneEnabled)
            startPatroll();
        updateUI();
    }

    public void callTspRIOnClusters() {
        hideLinks();
        listClusterLink.clear();
        List<Link> linkList;
        for(Cluster cluster: listCluster) {
            linkList = callTspRI(cluster.getListVertex(), cluster.getColor());
            listClusterLink.add(linkList);
        }
        if(droneEnabled)
            startPatroll();
        updateUI();
    }

    public void callTspLPOnClusters() {
        hideLinks();
        listClusterLink.clear();
        List<Link> linkList;
        for(Cluster cluster: listCluster) {
            linkList = callTspLP(cluster.getListVertex(), cluster.getColor());
            listClusterLink.add(linkList);
        }
        if(droneEnabled)
            startPatroll();
        updateUI();
    }

    public void startPatroll() {
        if(listDrone.size() == listClusterLink.size()) {
            for(int i=0; i < listDrone.size(); i++) {
                listDrone.get(i).setLinkList(listClusterLink.get(i));
            }
        }
    }

    public void performKeans(int nbAgents) {
        callKmeans(nbAgents);
    }

    private void callKmeans(int nbAgents) {
        mapIdNodes = new HashMap<Long, Node>();
        for(Node node: topo.getNodes()) {
            if(node.getClass() != Drone.class)
                mapIdNodes.put((long)node.getID(),node);
        }


        List<Vertex> listVertex = getListVertex();
        try {
            listCluster = kMeansAlgo.doCluster(listVertex, nbAgents);
            for(Cluster cluster: listCluster) {
                Color color = ColorUtils.getRandomColor();
                cluster.setColor(color);
                for(Vertex vertex: cluster.getListVertex()) {
                    mapIdNodes.get(vertex.getUid()).setColor(color);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<Link> callTspLP(List<Vertex> vertexList, Color color) {
        List<Edge> result = solverLP.solveTSP(vertexList);
        return displayResultTsp(result, color);
    }

    private List<Link> callTspRI(List<Vertex> listVertex, Color color) {
        List<Edge> result = solverRI.solveTSP(listVertex);
        return displayResultTsp(result, color);
    }

    private List<Link> callTsp2A(List<Vertex> vertexList, Color color) {
        List<Edge> resultE = solver2A.solveTSP(vertexList);
        return displayResultTsp(resultE, color);
    }

    private List<Link> displayResultTsp(List<Edge> resultE, Color color) {
        List<String> result = new ArrayList<>();
        List<Link> linkList = new ArrayList<>();

        for(Edge edge: resultE) {
            result.add(edge.getUid());
        }

        for (Link link : topo.getLinks()) {
            String s = link.endpoint(0).getID() + "_" + link.endpoint(1).getID();
            if(result.contains(s)) {
                link.setColor(color);
                link.setWidth(3);
                linkList.add(link);
            }
        }
        return linkList;
    }

    private List<Vertex> getListVertex() {
        List<Node> lNode = topo.getNodes();
        List<Vertex> lVertex = new ArrayList<>();
        for(Node node: lNode) {
            if(node.getClass() != Drone.class)
                lVertex.add(d2AHelper.nodeToVertex(node));
        }
        return lVertex;
    }

    @Override
    public void onLinkAdded(Link link) {
        link.setWidth(0);
    }

    @Override
    public void onLinkRemoved(Link link) {

    }

    @Override
    public void onCommand(String s) {
//        hideLinks();
        if(CMD_CLEAR_NODES.equals(s)) {
            clearAllNodes();
        } else if(CMD_ENABLE_DISABLE_DRONE.equals(s)) {
            setDroneEnabled(!isDroneEnabled());
        } else if(CMD_COMPUTE_RATIO.equals(s)) {
            computeAndDisplayRatio();
        }
        updateUI();
    }

    private void clearAllNodes() {
        for(Node node: topo.getNodes()) {
            if(node.getClass() != Drone.class)
                topo.removeNode(node);
        }
        listCluster.clear();
    }

    private void hideLinks() {
        for (Link link : topo.getLinks()) {
            link.setWidth(0);
        }
    }

    /**
     * Create nb Agents in starting positionnbAgents
     */
    public void updateAgents() {
        clearDrones();
        if(droneEnabled) {
            //On ajuste le nombre de drones si nécessaire
            if (listDrone.size() != nbAgents) {
                while (listDrone.size() < nbAgents) {
                    listDrone.add(new Drone());
                }
            }

            //Placement des drones en bas à gauche
            double height = topo.getDimensions().getHeight() - 30;
            Drone drone;
            for (int i = 0; i < listDrone.size(); i++) {
                drone = listDrone.get(i);
                topo.addNode(20 + (i * 45), height, drone);
            }
        }
    }

    private void clearDrones() {
        for(Drone drone:listDrone) {
            topo.removeNode(drone);
        }
        listDrone.clear();
    }

    public boolean isDroneEnabled() {
        return droneEnabled;
    }

    public void setDroneEnabled(boolean droneEnabled) {
        this.droneEnabled = droneEnabled;
        if(droneEnabled) {
            updateAgents();
        } else {
            clearDrones();
        }
    }

    public int getNbAgents() {
        return nbAgents;
    }

    public void setNbAgents(int nbAgents) {
        this.nbAgents = nbAgents;
    }

    private void computeAndDisplayRatio() {

        List<Edge> edgeListLP, edgeListRI, edgeList2A;
        double optiCost, riCost, twoACost;
        double ratio2A, ratioRi;
        String labelRatio;
        Node node;

        for(Cluster cluster: listCluster) {
            StringBuilder sBuild = new StringBuilder();

            edgeListLP = solverLP.solveTSP(cluster.getListVertex());
            edgeList2A = solver2A.solveTSP(cluster.getListVertex());
            edgeListRI = solverRI.solveTSP(cluster.getListVertex());

            optiCost    = Edge.getCost(edgeListLP);
            riCost      = Edge.getCost(edgeListRI);
            twoACost    = Edge.getCost(edgeList2A);

            ratio2A = twoACost / optiCost;
            ratioRi = riCost / optiCost;

            sBuild.append("\nTwo approx algo : ").append(df.format(twoACost));
            sBuild.append("\t\tratio : ").append(df.format(ratio2A));
            sBuild.append('\n');
            sBuild.append("Random insertion : ").append(df.format(riCost));
            sBuild.append("\t\tratio : ").append(df.format(ratioRi));
            sBuild.append('\n');
            sBuild.append("Optimum : ").append(df.format(optiCost));

            labelRatio = "2Apx : " + df.format(ratio2A) + " RI : " + df.format(ratioRi);

            for(Vertex vertex : cluster.getListVertex()) {
                node = mapIdNodes.get(vertex.getUid());
                node.setState(labelRatio);
            }
            System.out.println(sBuild.toString());
        }
    }
}
