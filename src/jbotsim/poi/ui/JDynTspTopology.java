package jbotsim.poi.ui;

import jbotsim.Link;
import jbotsim.Node;
import jbotsim.Topology;
import jbotsim.event.ClockListener;
import jbotsim.event.ConnectivityListener;
import jbotsim.poi.node.DynTspDrone;
import jbotsim.ui.CommandListener;
import jbotsim.ui.JTopology;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by vklein on 13/10/15.
 */
public class JDynTspTopology extends JTopology implements ConnectivityListener,CommandListener, ClockListener {

    //Constantes
    private static final String CMD_SOLVETSP_RI             = "Solve TSP - Random Insertion";
    private static final String CMD_DYNTSP                  = "Enable/Disable dynamic nodes";
    private static final String CMD_CLEAR_NODES             = "Clear all nodes";

    /*
     * Etat INIT : état initial
     * Etat RUNNING : un tsp résolut le drone le parcours
     * Etat DYNAMIC : un tsp résolut le drone le parcours, des sommet apparaissent et disparaissent régulierement
     * et doivent être inclus dans le parcours du drone.
     */
    private enum State { INIT, RUNNING, DYNAMIC }

    private DynTspDrone drone;
    private State state;
    private Random random = new Random();

    public JDynTspTopology() {
        this(new Topology());
    }

    public JDynTspTopology(Topology topology) {
        super(topology);
        state = State.INIT;
        topology.setCommunicationRange(1000);
        topology.addConnectivityListener(this);
        addCommandListener(this);
        topology.addClockListener(this, 800);


        //Commands
        addCommand(CMD_SOLVETSP_RI);
        addCommand(CMD_DYNTSP);
        addCommand(CMD_CLEAR_NODES);

        drone = new DynTspDrone();
        this.topo.addNode(30, 30, drone);
    }

    @Override
    public void onCommand(String cmd) {
        if(CMD_SOLVETSP_RI.equals(cmd)) {
            hideLinks();
            List<Node> nodeList = new ArrayList<>();
            for(Node node: topo.getNodes()) {
                if(node.getClass() != DynTspDrone.class) {
                    nodeList.add(node);
                }
            }
            drone.addNodes(nodeList);
            state = State.RUNNING;
        } else if(CMD_DYNTSP.equals(cmd)) {
            if(state == State.RUNNING) {
                state = State.DYNAMIC;
            } else if(state == State.DYNAMIC) {
                state = State.RUNNING;
            }
        } else if(CMD_CLEAR_NODES.equals(cmd)) {
            state = State.INIT;
            clearAllNodes();
        }
    }

    private void clearAllNodes() {
        for(Node node: topo.getNodes()) {
            if(node.getClass() != DynTspDrone.class)
                topo.removeNode(node);
            else {
                drone.setLocation(30,30);
                drone.stop();
            }

        }
    }

    private void hideLinks() {
        for (Link link : topo.getLinks()) {
            link.setWidth(0);
        }
    }

    @Override
    public void onLinkAdded(Link link) {
        link.setWidth(0);
    }

    @Override
    public void onLinkRemoved(Link link) {

    }

    @Override
    public void onClock() {
        if(state == State.DYNAMIC) {
            hideLinks();
            //if(random.nextBoolean()) {
            if(false) {
                //Add node
                Node node = addRandomNode();
                drone.addNode(node);
            } else {
                List<Node> nodeList = topo.getNodes();
                nodeList.remove(drone);

                //Remove random node
                int idx = random.nextInt(nodeList.size());
                Node nodeToRemove = nodeList.get(idx);
                drone.removeNode(nodeToRemove);
                this.topo.removeNode(nodeToRemove);
            }
        }
    }

    /**
     * Add a new node to topology with random location
     * @return the newly created node.
     */
    private Node addRandomNode() {
        int y = random.nextInt(this.getHeight());
        int x = random.nextInt(this.getWidth());
        Node res = new Node();
        this.topo.addNode(x, y, res);
        return res;
    }
}
