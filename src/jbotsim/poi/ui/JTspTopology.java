package jbotsim.poi.ui;

import daisie.algo.Edge;
import daisie.algo.Vertex;
import daisie.algo.clustering.Cluster;
import daisie.algo.clustering.ClusterAlgorithm;
import daisie.algo.clustering.KMeansAlgo;
import jbotsim.poi.node.Drone;
import daisie.algo.tsp.*;
import jbotsim.Link;
import jbotsim.Node;
import jbotsim.Topology;
import jbotsim.event.ConnectivityListener;
import jbotsim.ui.CommandListener;
import jbotsim.ui.JTopology;
import jbotsim.utils.ColorUtils;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by vklein on 09/09/15.
 */
public class JTspTopology extends JTopology implements CommandListener, ConnectivityListener {
    //Constantes
    private static final String CMD_SOLVETSP_LP         = "Solve TSP - Linear Programing";
    private static final String CMD_SOLVETSP_2APX       = "Solve TSP - 2 approx Algorithms";
    private static final String CMD_CLUSTERING_KMEANS   = "Clustering Kmeans";
    private static final String CMD_TSPLP_ONCLUSTER     = "Solve TSP LP on clusters";
    private static final String CMD_TSP2APX_ONCLUSTER   = "Solve TSP 2Approx on clusters";

    private TspSolver solverLP;
    private TspSolver solver2A;
    private ClusterAlgorithm kMeansAlgo;
    private HashMap<Long, Node> mapIdNodes;
    private List<Cluster> listCluster = new ArrayList<>();

    public JTspTopology() {
        this(new Topology(800, 600));
    }

    public JTspTopology(Topology topology) {
        super(topology);
        topology.setCommunicationRange(1000);
        topology.addConnectivityListener(this);
        //Commands
        addCommand(CMD_SOLVETSP_LP);
        addCommand(CMD_SOLVETSP_2APX);
        addCommand(CMD_CLUSTERING_KMEANS);
        addCommand(CMD_TSP2APX_ONCLUSTER);
        addCommand(CMD_TSPLP_ONCLUSTER);
        addCommandListener(this);
        initTspSolver();
    }

    private void initTspSolver() {
        solverLP = new TspLP();
        solver2A = new TspTwoApprox4Metric();
        kMeansAlgo = new KMeansAlgo();
    }

    @Override
    public void onCommand(String cmd) {
        hideLinks();
        if(CMD_SOLVETSP_LP.equals(cmd)) {
            callTspLP(getListVertex(), Color.BLACK);
        } else if(CMD_SOLVETSP_2APX.equals(cmd)) {
            callTsp2A(getListVertex(), Color.BLACK);
        } else if(CMD_CLUSTERING_KMEANS.equals(cmd)) {
            callKmeans();
        } else if(CMD_TSP2APX_ONCLUSTER.equals(cmd)) {
            callTsp2ApxOnClusters();
        } else if(CMD_TSPLP_ONCLUSTER.equals(cmd)) {
            callTspLPOnClusters();
        }
        updateUI();
    }

    private void callTsp2ApxOnClusters() {
        for(Cluster cluster: listCluster) {
            callTsp2A(cluster.getListVertex(), cluster.getColor());
        }
    }

    private void callTspLPOnClusters() {
        for(Cluster cluster: listCluster) {
            callTspLP(cluster.getListVertex(), cluster.getColor());
        }
    }

    private void callKmeans() {
        mapIdNodes = new HashMap<>();
        for(Node node: topo.getNodes()) {
            if(node.getClass() != Drone.class) {
                mapIdNodes.put((long) node.getID(), node);
            }
        }


        List<Vertex> listVertex = getListVertex();
        try {
            listCluster = kMeansAlgo.doCluster(listVertex, 3);
            for(Cluster cluster: listCluster) {
                Color color = ColorUtils.getRandomColor();
                cluster.setColor(color);
                for(Vertex vertex: cluster.getListVertex()) {
                    mapIdNodes.get(vertex.getUid()).setColor(color);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callTspLP(List<Vertex> vertexList, Color color) {
        List<Edge> result = solverLP.solveTSP(vertexList);
        displayResultTsp(result, color);
    }

    private void callTsp2A(List<Vertex> vertexList, Color color) {
        List<Edge> resultE = solver2A.solveTSP(vertexList);
        displayResultTsp(resultE, color);
    }

    private void displayResultTsp(List<Edge> resultE, Color color) {
        List<String> result = new ArrayList<String>();

        for(Edge edge: resultE) {
            result.add(edge.getUid());
        }

        for (Link link : topo.getLinks()) {
            String s = link.endpoint(0) + "_" + link.endpoint(1);
            if(result.contains(s)) {
                link.setColor(color);
                link.setWidth(3);
            }
        }
    }

    private void hideLinks() {
        for (Link link : topo.getLinks()) {
            link.setWidth(0);
        }
    }

    @Override
    public void onLinkAdded(Link link) {
        link.setWidth(0);
    }

    @Override
    public void onLinkRemoved(Link link) {
    }

    private List<Vertex> getListVertex() {
        List<Node> lNode = topo.getNodes();
        List<Vertex> lVertex = new ArrayList<>();
        for(Node node: lNode) {
            if(node.getClass() != Drone.class)
                lVertex.add(new Vertex(node.getID(), node.getX(), node.getY()));
        }
        return lVertex;
    }
}
