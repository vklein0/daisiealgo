package quiknddirty;

import daisie.algo.Edge;
import daisie.algo.Vertex;
import daisie.algo.tsp.TspLP;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by vklein on 20/09/16.
 */
public class Tirs4AlgoMob {
    public static void main(String[] args) throws IOException {
        int nbTirs = 1;
        int nbVertices = 15;
        int MAX_X = 300;
        int MAX_Y = 300;
        double totCost = 0;

        List<Vertex> vertexList; //Entrée
        List<Edge> edgeList; //Sortie, solution du tsp

        TspLP tspLP = new TspLP();
        String fileName = "listePointSolutionTsp.txt";
        FileWriter writer = new FileWriter(fileName);

        for(int i=0; i < nbTirs; i++) {

            vertexList = new ArrayList<>();
            Vertex vertex;
            double x,y;
            for(int j =0; j < nbVertices; j++) {
                x = ThreadLocalRandom.current().nextInt(10, MAX_X-10);
                y = ThreadLocalRandom.current().nextInt(10, MAX_Y-10);
                vertex = new Vertex(j, x, y);
                vertexList.add(vertex);
                writer.append("topology.addNode"+vtostr(vertex)+";\n");
            }
            edgeList = tspLP.solveTSP(vertexList);
            totCost += Edge.getCost(tspLP.solveTSP(vertexList));

            //Sauvegarde des points de la solution dans l'ordre.
            Edge one = edgeList.get(0);
            for(Edge edge : edgeList) {

                writer.append(edge.getSource() + " - " + edge.getDestination() + " : " + vtostr(edge.getSource()) + "-"
                        + vtostr(edge.getDestination()) + "\n");
            }
        }
        writer.append("\nCout : " + totCost );
        System.out.println("\nCout : " + totCost);
        writer.flush();
        writer.close();
    }

    private static String vtostr(Vertex vertex) {
        return "("+vertex.getX()+ ", " + vertex.getY() + ")";
    }
}
